<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
	<?php include 'template/head.php'; ?>
    <title><?= $title ?></title> 
</head>

<body>
    <div id="wrapper">
    	<?php include 'template/navbar.php'; ?>
        <?php include 'template/sidebar.php'; ?>
        <!-- /. NAV SIDE  -->
      
		<div id="page-wrapper">
	        <div id="page-inner">
	        	<?= $page ?>
	    	</div>
	    	<?php include 'template/footer.php'; ?>
		</div>
	</div>
<?php include 'template/script.php'; ?> 
</body>

</html>