<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="card">
            <div class="card-action">
                 Kelola Buku
                <?php if($this->session->flashdata('success')): ?>
                <div class="alert alert-primary" role="alert">
                <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('danger')): ?>
                <div class="alert alert-danger" role="alert">
                <?php echo $this->session->flashdata('danger'); ?>
                </div>
                <?php endif; ?>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="center">Judul Buku</th>
                                <th class="center">Penulis</th>
                                <th class="center">Tahun Terbit</th>
                                <th class="center">Jenis Buku</th>
                                <th class="center">Kategori Buku</th>
                                <th class="center">Instansi</th>
                                <th class="center">Keterangan</th>
                                <th class="center">File</th>
                                <th class="center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach ($daftar_buku as  $buku) :?>
                            <tr class="odd gradeA">
                                <td><?= $no ?></td>
                                <td><?= $buku->judul ?></td>
                                <td><?= $buku->penulis ?></td>
                                <td><?= $buku->thn_terbit ?></td>
                                <td><?= $buku->jenis_buku ?></td>
                                <td><?= $buku->kategori_buku ?></td>
                                <td><?= $buku->instansi ?></td>
                                <td><?= $buku->keterangan ?></td>
                                <td class="center">
                                    <?php if($buku->upload != ""){ ?>
                                        <a href="<?= base_url().'upload_buku/'.$buku->upload;?>">Lihat Buku</a>
                                    <?php } ?>
                                </td>
                                <td class="center"><a href="<?php echo base_url() ?>buku/admin_edit/<?= $buku->id ?>"><button class="btn btn-xs btn-warning" style="font-size: 9px">Edit</button></a>&nbsp;<a href="<?php echo base_url() ?>buku/delete/<?= $buku->id ?>"><button class="btn btn-xs btn-danger" style="font-size: 9px">Hapus</button></a></td>
                            </tr>
                            <?php $no++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>
