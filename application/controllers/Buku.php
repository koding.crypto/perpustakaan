<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {
    public function index(){
    	$this->check_login();
    	$data['title'] 	= 'halaman dashboard';
    	$elemen_web = 'hai';
    	$data['page']	=$this->load->view('web/dashboard',$elemen_web, true);
        $this->load->view('template',$data);
    }
    public function admin_management(){
    	$this->check_login();
    	$this->check_admin();
    	$data['title'] 	= 'Tambahkan Buku';
    	if(!$this->input->post('submit')){
            $elemen_web = 'hai';
	    	$data['page']	=$this->load->view('buku/admin_management',$elemen_web, true);
	        $this->load->view('template',$data);
        } else {
        	//	id	judul	bidang	jenis_data	sumber	periode	level	ketersediaan	tanggal_rilis	url	upload
            $data = [
	            'judul' => $this->input->post('judul'),
	            'penulis' => $this->input->post('penulis'),
	            'thn_terbit' => $this->input->post('thn_terbit'),
	            'instansi' => $this->input->post('instansi'),
                'kategori_buku' => $this->input->post('kategori_buku'),
                'jenis_buku' => $this->input->post('jenis_buku'),
                'keterangan' => $this->input->post('keterangan'),
                'waktu' => date("Y-m-d")
	        ];

       		if (!empty($_FILES['upload_buku']['name'])) {
	        	$config = array(
				'file_name' => $this->input->post('judul'),
				'upload_path' => "./upload_buku/",
				'allowed_types' => "pdf|doc|docx|xlsx",
				'overwrite' => TRUE,
				'max_size' => 0,
				'max_width' => 0,
				'max_height' => 0,
				'encrypt_name' => TRUE
				);
				$this->load->library('upload', $config);

				if($this->upload->do_upload('upload_buku') == 1){
					$bdata = $this->upload->data();
					$data['upload'] =  $bdata['file_name'];
				}
			} else{
				$data['upload'] =  "";
			}

			if($this->db->insert('buku', $data) == 1){
				$this->session->set_flashdata('success', 'Berhasil Menambahkan Buku');
				redirect('buku/admin_daftar');
			}     
        }
    }
  
    public function admin_daftar(){
    	$this->check_login();
    	$this->check_admin();
    	$data['title'] 	= 'Kelola Buku';
    	$elemen_web['daftar_buku'] = $this->db->order_by('id')->get('buku')->result();
    	$data['page']	=$this->load->view('buku/admin_daftar',$elemen_web, true);
        $this->load->view('template',$data);
    }

    public function admin_edit($id){
    	$this->check_login();
    	$this->check_admin();
    	$data['title'] 	= 'Update Data Buku';
    	if(!$this->input->post('submit')){
	    	$elemen_web['data_buku'] = $this->buku_model->get_buku_id($id);
	    	$data['page']	= $this->load->view('buku/admin_edit',$elemen_web, true);
	        $this->load->view('template',$data);
	    }
	    else{
	    	$id = $this->input->post('id');
	    	$data = [
                'judul' => $this->input->post('judul'),
                'penulis' => $this->input->post('penulis'),
                'thn_terbit' => $this->input->post('thn_terbit'),
                'instansi' => $this->input->post('instansi'),
                'kategori_buku' => $this->input->post('kategori_buku'),
                'jenis_buku' => $this->input->post('jenis_buku'),
                'keterangan' => $this->input->post('keterangan'),
            ];

	        if (!empty($_FILES['upload_buku']['name'])) {
	        	$link_file = './upload_buku/'.$upload_url;
	        	unlink($link_file);
	        	$config = array(
				'file_name' => $judul,
				'upload_path' => "./upload_buku/",
				'allowed_types' => "pdf|doc|docx|xlsx",
				'overwrite' => TRUE,
				'max_size' => 0,
				'max_width' => 0,
				'max_height' => 0,
				'encrypt_name' => TRUE
				);
				$this->load->library('upload', $config);
				if($this->upload->do_upload('upload_buku') == 1){
					$bdata = $this->upload->data();
					$data['upload'] =  $bdata['file_name'];
				}
			}
			if($this->db->where('id', $this->input->post('id'))->update('buku', $data) == 1){
				$this->session->set_flashdata('success', 'Berhasil Update Buku');
				redirect('buku/admin_daftar');
			}
	    }
    }

    public function cari(){
    	$this->check_login();
    	$data['title'] 	= 'Cari Buku';
    	$elemen_web['daftar_buku'] = $this->db->order_by('id')->get('buku')->result();
    	$data['page']	=$this->load->view('buku/cari',$elemen_web, true);
        $this->load->view('template',$data);
    }

    public function delete($id){
    	$this->check_login();
    	$this->check_admin();
        $this->db->where('id', $id)->delete('buku');
        $this->session->set_flashdata('success', 'Berhasil Menghapus Buku');
        redirect('buku/admin_daftar');
    }

    private function check_login(){
        // check login
        if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
    }
    private function check_admin(){
        // check login
        if($this->session->userdata('role') != 1){
            redirect('web');
        }
    }

}


		