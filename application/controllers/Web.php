<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {
    public function index(){
        $this->check_login();
        $data['title']  = 'Halaman Dashboard';

        //waktu sekarang
        $waktu_sekarang     = date("Y-m-d");
        $waktu_sebelumnya   = date("Y-m-d", strtotime("-1 week"));

        $this->db->where('waktu BETWEEN "'. date('Y-m-d', strtotime($waktu_sebelumnya)). '" and "'. date('Y-m-d', strtotime($waktu_sekarang)).'"');
        $query = $this->db->get('buku');
        $elemen_web['jumlah_buku_seminggu'] = $query->num_rows();

        $this->db->where('waktu BETWEEN "'. date('Y-m-d', strtotime($waktu_sebelumnya)). '" and "'. date('Y-m-d', strtotime($waktu_sekarang)).'"');
        $query = $this->db->get('akses');
        $elemen_web['jumlah_anggota_seminggu'] = $query->num_rows();


        $query = $this->db->get('buku');
        $elemen_web['jumlah_buku'] = $query->num_rows();

        $this->db->where(['role' => 0]);
        $query = $this->db->get('akses');
        $elemen_web['jumlah_anggota'] = $query->num_rows();

        $data['page']   =$this->load->view('web/dashboard',$elemen_web, true);
        $this->load->view('template',$data);
    }
    private function check_login(){
        // check login
        if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
    }

}
