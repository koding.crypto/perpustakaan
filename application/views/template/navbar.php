<nav class="navbar navbar-default top-navbar" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle waves-effect waves-dark" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand waves-effect waves-dark" href="<?= base_url().'web'; ?>"><i class="fa-books"></i>PERPUSTAKAAN</a>
    	
    	<div id="sideNav" href=""><i class="material-icons dp48">toc</i>
    	</div>
    	</div>

    <ul class="nav navbar-top-links navbar-right"> 
    	  <li><a href="<?= base_url().'users/logout'; ?>"><button class="btn btn-xs btn-danger" style="height: 25px; margin-right: 20px">Logout</button></a></li>
    </ul>
</nav>